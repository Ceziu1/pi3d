# Wymagania systemowe

- Node.JS w wersji > 5
- przeglądarka z obsługą silnika WebGL

# Strona serwerowa

Użyj polecenia `node server.js` w głównym folderze projektu aby uruchomić serwer. Domyślnym dla aplikacji jest port 3000

# Strona Klienta

Istnieją 2 typy klientów:

- Klient wyświetlający
- Klient zarządzający

# Klent wyświetlający

Uruchamiany na przygotowanym urządzeniu wyposarzonym w wyświetlacz i pryzmat. W przeglądarce w trybie pełnoekranowym 
należy udać się pod adres ip serwera a na ekranie ujrzymy informacje o prawidłowym połączeniu się urządzeń. Na dowolnie innym urządzeniu,
udajemy się pod ten sam adres, ale po numerze portu dopisujemy ścieżkę `/ui` dzięki czemu dostaniemy dostęp do panelu sterowania urządzenia.

# Sterowanie

Na stronie wybory katalogu z modelami należy na urządzeniu mobilnym przytrzymać, a na komputerze użyć PPM aby przejść głebiej w hierarchii.
Po wyborze interesującego nas pliku kliknąć guzik _Projekcja_. Zostaniemy przeniesieni do strony dającej na możliwość:

- obracania
- skalowania
- przesuwania
- zmiany tekstury (tylko niektóre modele tj. obrazy)

Do każdego modelu dostępne jest menu pomoc, które uruchomi wirtualną asystentkę, która będzie w stanie dostarczyć informacji na temat
obecnie wyświetlanego modelu.

W przypadku wybrania pliku wideo zamiast wyżej wymienionych kontrolek otrzymamy dostęp do odtwarzacza umożliwiającego:

- zatrzymywanie/ odtwarzanie
- przewijanie (nie działa)
- dodawanie kolejnego wideo do kolejki odtwarzania

# Do naprawienia

- Moduł odpowiedzialny za odtwarzanie filmów obsługuje tylko jego uruchomienie lub wstrzymanie (nie działa przewijanie).
- PiDi ma bardzo ograniczoną funkcjionalność. Działa tylko w menu jako samouczek.
- Błąd wczytywania pamięci zewnętrznej w celu rozszerzenia biblioteki o nowe modele/filmy.
var socketio = io();
var dir, ui, prevDeegre, $verticalSlider, $verticalSlider1, $verticalSlider2, $verticalLink, $verticalLink1, $verticalLink2;
var pidiDialogueIndex= 0;
/*-----------------------EVENTS--------------------------*/
$(document).ready(function () {
	$("#upload").click(function () {
		if ($(".todo-done").text() == "") {
			return;
		}
		// if($(".todo-done").text().indexOf(".conf")>= 0){
			pidiDialogueIndex= 0;
			socketio.emit("upload", {
				"config": $(".todo-done").text()
			});
		// }
	})

	$("#help").click(function () {
		socketio.emit("showPiDI", {});
		piDI_options = piDI_base;
		showOptions();
		$("#helpDiv").show(500);
	})

	$("#piDIInfo").click(function(){//informacje do danego modelu
		socketio.emit("showPiDI", {});
		socketio.emit("playPiDIdialogue", {
			file: "modelsAudio/" + ui.audio[pidiDialogueIndex]
		});//TODO PO ZAKONCZENIU PODAWANIA INFORMACJI SCHOWAĆ PIDI
	})

	$("#helpDiv").click(function () {
		console.log(event)
		var img = $('#helpDiv img:first-child');
		var offset = img.offset();
		var center_x = (offset.left) + (img.width() / 2);
		var center_y = (offset.top) + (img.height() / 2);
		var mouse_x = event.pageX;
		var mouse_y = event.pageY;
		var radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
		var degree = (radians * (180 / Math.PI) * -1) + 180;
		img.css('-moz-transform', 'rotate(' + degree + 'deg)');
		img.css('-webkit-transform', 'rotate(' + degree + 'deg)');
		img.css('-o-transform', 'rotate(' + degree + 'deg)');
		img.css('-ms-transform', 'rotate(' + degree + 'deg)');
		helpMenuOptions(degree);
	});

	$("#defaultView").click(function () {
		updateProperties();
		changeSceneParams("zoom", parseInt($("#zoom a").css("bottom")) / 200);
		changeSceneParams("moveX", parseInt($("#moveX a").css("bottom")) / 200);
		changeSceneParams("moveY", parseInt($("#moveY a").css("bottom")) / 200);
		changeSceneParams("rotate", parseInt($("#rotate a").css("bottom")) / 200);
		changeSceneParams("offset", parseInt($("#offset a").css("bottom")) / 200);
	})
})
/*-----------------------SOCKET--------------------------*/
//odczytanie konfiguracji
socketio.on("renderConfigForUI", function (data) {
	localStorage.removeItem("model");
	localStorage.setItem("model", data.config);
	while(localStorage.getItem("model") == null)
		continue;
	window.location.href = "opcje.html"
})
//Odczytanie obecnego stanu odtwarzacza filmów

//Tworzenie listy plikow i katalogow
socketio.on("dirList", function (data) {
	$("#dirList").html("")
	for (var i = 0; i < data.list.length; i++) {
		if (data.list[i].attr == true) {
			$("#dirList").append("<li class='animated fadeInDown directory fileManager'><div class='todo-content'><div class='todo-icon fui-folder'></div><h6 class='todo-name'>" + data.list[i].file + "</h6></div></li>")
		}
		else if(data.list[i].file.indexOf(".mp4") >= 0) {//<div class='todo-icon fui-document'></div>
			$("#dirList").append("<li class='animated fadeInDown fileManager'><div class='todo-content'><div class='todo-icon' style='background: url(" + "img/custom_icons/media.png" + ");background-size: 40px 40px; width: 40px; height: 40px'></div><h6 class='todo-name'>" + data.list[i].file + "</h6></div></li>")
		} else{
			$("#dirList").append("<li class='animated fadeInDown fileManager'><div class='todo-content'><div class='todo-icon fui-document'></div><h6 class='todo-name'>" + data.list[i].file + "</h6></div></li>")
		}
	}
	//doubleClick na folderze przechodzi dalej w strukturze
	for (var i = 0; i < document.getElementsByClassName("directory").length; i++) {
		document.getElementsByClassName("directory")[i].addEventListener("contextmenu", function () {
			directoryList(this.innerText.trim(), false);
		}, false);
	}
	//okodzenie wybrania tylko jednej pozycji z listy plikow
	for (var i = 0; i < document.getElementsByClassName("fileManager").length; i++) {
		document.getElementsByClassName("fileManager")[i].addEventListener("click", function () {
			if (!$(this).hasClass("directory")) { //uruchamianie guzika do porjekcji
				$("#upload").removeClass("disabled")
			}
			else {
				$("#upload").addClass("disabled")
			}
			for (var j = 0; j < document.getElementsByClassName("fileManager").length; j++) {
				if (document.getElementsByClassName("fileManager")[j].className.indexOf("directory") >= 0) {
					document.getElementsByClassName("fileManager")[j].className = "fileManager directory";
				}
				else {
					document.getElementsByClassName("fileManager")[j].className = "fileManager";
				}
			}
		}, false);
	}
});
/*-----------------------FUNCTIONS--------------------------*/
function directoryList(path, root) {
	socketio.emit("path", {
		path: path,
		defaultDir: root //jezeli kliknięta ściezka domyślna to true
	});
}

function changeSceneParams(type, info) {
	socketio.emit("updateInfo", {
		updateType: type, 
		data: info
	});
}

function updateTexture(data, index) {
	pidiDialogueIndex= index;
	socketio.emit("updateTexture", {
		texture: data
	});
	if(ui.objects[index].offset == true){
		$("#offsetHide").show();
		$verticalSlider4 = $("#offset");
		if ($verticalSlider4.length) {
			$verticalSlider4.slider({
				min: 0
				, max: ui.objects[index].offsetX * 100
				, value: 0
				, orientation: "vertical"
				, range: "min"
			}).addSliderSegments($verticalSlider4.slider("option").max, "vertical");
		}
		$("#offset a").mousemove(function () {
			changeSceneParams("offset", parseInt($(this).css("bottom")) / 200);
		})
	} else{
		try{
			$verticalSlider4.empty();
			$("#offsetHide").hide();
		} catch(err){
			console.log(err)
		}
	}
}

function updateProperties() { //budowanie GUI w menu opcje
	if(JSON.parse(localStorage.getItem("model").indexOf(".mp4")>= 0)){
		$("#controls").html("<object data='videoControls.html'/>")
		return;
	}
	else if(JSON.parse(localStorage.getItem("model")) == null){
		alert("Nie wybrano żadnego modelu")
		window.location.href = "foldery.html"
	}
	ui = JSON.parse(localStorage.getItem("model"))
	var step = ui.option.step
	var zoom = ui.option.zoom
	var positionZ = ui.option.positionZ
	var positionY = ui.option.positionY
	var objects = ui.objects
	$("#dropdown").empty();
	for (var i = 0; i < objects.length; i++) {
		$("#dropdown").append("<li onclick='updateTexture(this.innerText, $(this).index())'><a href='#' class='textures'>" + objects[i].name + "</a></li>")
	}
	$verticalSlider = $("#zoom");
	if ($verticalSlider.length) {
		$verticalSlider.slider({
			min: -zoom / step
			, max: zoom / step
			, value: 0
			, orientation: "vertical"
			, range: "min"
		}).addSliderSegments($verticalSlider.slider("option").max, "vertical");
	}
	$("#zoom a").mousemove(function () {
		changeSceneParams("zoom", parseInt($(this).css("bottom")) / 200);
	})
	$verticalSlider1 = $("#moveX");
	if ($verticalSlider1.length) {
		$verticalSlider1.slider({
			min: -positionZ / step
			, max: positionZ / step
			, value: 0
			, orientation: "vertical"
			, range: "min"
		}).addSliderSegments($verticalSlider1.slider("option").max, "vertical");
	}
	$("#moveX a").mousemove(function () {
		changeSceneParams("moveX", parseInt($(this).css("bottom")) / 200);
	})
	$verticalSlider2 = $("#moveY");
	if ($verticalSlider2.length) {
		$verticalSlider2.slider({
			min: -positionY / step
			, max: positionY / step
			, value: 0
			, orientation: "vertical"
			, range: "min"
		}).addSliderSegments($verticalSlider2.slider("option").max, "vertical");
	}
	$("#moveY a").mousemove(function () {
		changeSceneParams("moveY", parseInt($(this).css("bottom")) / 200);
	})
	$verticalSlider3 = $("#rotate");
	if ($verticalSlider3.length) {
		$verticalSlider3.slider({
			min: 0
			, max: 360
			, value: 180
			, orientation: "vertical"
			, range: "min"
		}).addSliderSegments($verticalSlider3.slider("option").max, "vertical");
	}
	$("#rotate a").mousemove(function () {
		changeSceneParams("rotate", parseInt($(this).css("bottom")) / 200);
	})
	$(".ui-slider-segment").css({
		visibility: "hidden"
	});
}
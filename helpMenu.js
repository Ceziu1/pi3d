var piDI_options = []
var piDI_base = [
  ["", ""],
  ["Wyjdź", "Samouczek"],
  ["", ""]
]
var piDI_tutorial = [
  ["", "Technologia"],
  ["Wróć", "Multimedia"],
  ["", "Sterowanie"]
]
var prevDegree = 0;
var options = $('#helpDiv img:last-child');

function showOptions() {
  $("#options").empty();
  for (var i = 0; i < piDI_options.length; i++) {
    for (var j = 0; j < piDI_options[i].length; j++) {
      if (j % 2 == 0) {
        $("#options").append("<span>" + piDI_options[i][j] + "</span>")
      } else
        $("#options").append("<span>" + piDI_options[i][j] + "</span><br><br>")
    }
  }
}

function helpMenuOptions(degree) {
  if (degree > 0 && degree <= 60 && piDI_options[0][1] != "") {
    if (prevDegree > 0 && prevDegree <= 60) {
      socketio.emit("playPiDIdialogue", {
        file: "piDI/audio/" + piDI_options[0][1] + ".mp3"
      });
    }
    else
      options.attr('src', 'piDI/circle_top_right.png');
  }
  else if (degree > 60 && degree <= 120 && piDI_options[1][1] != "") {
    if (prevDegree > 60 && prevDegree <= 120){
      socketio.emit("playPiDIdialogue", {
        file: "piDI/audio/" + piDI_options[1][1] + ".mp3"
      });
      switch (piDI_options[1][1]) {
        case "Samouczek":
          piDI_options = piDI_tutorial;
          showOptions();
          break;
        default:
          break;
      }
    }
    else
      options.attr('src', 'piDI/circle_middle_right.png')
  }
  else if (degree > 120 && degree <= 180 && piDI_options[2][1] != "") {
    if (prevDegree > 120 && prevDegree <= 180) {
      socketio.emit("playPiDIdialogue", {
        file: "piDI/audio/" + piDI_options[2][1] + ".mp3"
      });
    }
    else
      options.attr('src', 'piDI/circle_bottom_right.png')
  }
  else if (degree > 180 && degree <= 240 && piDI_options[2][0] != "") {
    if (prevDegree > 180 && prevDegree <= 240)
      console.log("wchodzwe dalej")
    else
      options.attr('src', 'piDI/circle_bottom_left.png')
  }
  else if (degree > 240 && degree <= 300 && piDI_options[1][0] != "") {
    if (prevDegree > 240 && prevDegree <= 300) {
      socketio.emit("playPiDIdialogue", {
        file: "piDI/audio/" + piDI_options[1][0] + ".mp3"
      });
      switch (piDI_options[1][0]) {
        case "Wyjdź":
          $("#helpDiv").hide(500);
          // socketio.emit("hidePiDI", {});
          break;
        case "Wróć":
          piDI_options = piDI_base;
          showOptions();
          break;
        default:
          break;
      }
    }
    else
      options.attr('src', 'piDI/circle_middle_left.png')
  }
  else if (degree > 300 && degree <= 360 && piDI_options[0][0] != "") {
    if (prevDegree > 300 && prevDegree <= 360)
      console.log("wchodzwe dalej")
    else
      options.attr('src', 'piDI/circle_top_left.png')
  }
  prevDegree = degree;
}
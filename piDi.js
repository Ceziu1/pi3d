function loadBasePiDI() {
  for (var i = 0; i < scene.children.length; i++)
    scene.children[i].visible = false;
  loader.load("piDI/model/piDI.json", function (geometry, material) {
    material[1].opacity = 0.05
    var materials = new THREE.MeshFaceMaterial(material);
    piDiBase = new THREE.Mesh(geometry, materials);
    piDiBase.scale.set(5, 5, 5);
    scene.add(piDiBase);
    // camera.lookAt(piDiBase.position);
    PiDIsilenceAnimation();
  })
}

function closePiDI() {
  for (var i = 0; i < scene.children.length; i++)
    scene.children[i].visible = true;
  try {
    scene.remove(piDiBase)
    scene.remove(piDiAnimation)
    scene.remove(piDiAnimation1)
    scene.remove(piDiAnimation2)
  } catch (err) {
    console.log(err);
  }
}

function PiDIsilenceAnimation() {
  try {
    scene.remove(piDiAnimation)
    scene.remove(piDiAnimation1)
    scene.remove(piDiAnimation2)
  } catch (err) {
    console.log(err)
  }
  loader.load('piDI/model/silence_final1.json', function (geometry, mat) {
    geometry.computeMorphNormals();
    matt = new THREE.MeshLambertMaterial({
      emissive: 0x74DDFB,
      color: 0x74DDFB,
      morphTargets: true,
      morphNormals: true,
      specular: 0xffffff,
      shininess: 10,
      transparent: true,
      shading: THREE.SmoothShading,
      vertexColors: THREE.FaceColors
    });
    piDiAnimation = new THREE.MorphAnimMesh(geometry, matt);
    piDiAnimation.rotateY(Math.PI)
    piDiAnimation.parseAnimations();
    piDiAnimation.scale.set(5, 5, 5);
    scene.add(piDiAnimation);
  });
  loader.load('piDI/model/silence_final2.json', function (geometry, mat) {
    geometry.computeMorphNormals();
    matt = new THREE.MeshLambertMaterial({
      emissive: 0x74DDFB,
      color: 0x74DDFB,
      morphTargets: true,
      morphNormals: true,
      specular: 0xffffff,
      shininess: 10,
      transparent: true,
      shading: THREE.SmoothShading,
      vertexColors: THREE.FaceColors
    });
    piDiAnimation1 = new THREE.MorphAnimMesh(geometry, matt);
    piDiAnimation1.rotateY(Math.PI)
    piDiAnimation1.parseAnimations();
    piDiAnimation1.scale.set(5, 5, 5);
    scene.add(piDiAnimation1);
  });
  loader.load('piDI/model/silence_final3.json', function (geometry, mat) {
    geometry.computeMorphNormals();
    matt = new THREE.MeshLambertMaterial({
      emissive: 0x74DDFB,
      color: 0x74DDFB,
      morphTargets: true,
      morphNormals: true,
      specular: 0xffffff,
      shininess: 10,
      transparent: true,
      shading: THREE.SmoothShading,
      vertexColors: THREE.FaceColors
    });
    piDiAnimation2 = new THREE.MorphAnimMesh(geometry, matt);
    piDiAnimation2.rotateY(Math.PI)
    piDiAnimation2.parseAnimations();
    piDiAnimation2.scale.set(5, 5, 5);
    scene.add(piDiAnimation2);
  });
  deltaMultiply = 700;
}

function PiDIspeakAnimation() {
  deltaMultiply = 300;
  try {
    scene.remove(piDiAnimation)
    scene.remove(piDiAnimation1)
    scene.remove(piDiAnimation2)
  } catch (err) {
    console.log(err)
  }
  loader.load('piDI/model/speak_final1.json', function (geometry, mat) {
    geometry.computeMorphNormals();
    matt = new THREE.MeshLambertMaterial({
      emissive: 0x74DDFB,
      color: 0x74DDFB,
      morphTargets: true,
      morphNormals: true,
      specular: 0xffffff,
      shininess: 10,
      transparent: true,
      shading: THREE.SmoothShading,
      vertexColors: THREE.FaceColors
    });
    piDiAnimation = new THREE.MorphAnimMesh(geometry, matt);
    piDiAnimation.rotateY(Math.PI)
    piDiAnimation.parseAnimations();
    piDiAnimation.scale.set(5, 5, 5);
    scene.add(piDiAnimation);
  });
  loader.load('piDI/model/speak_final2.json', function (geometry, mat) {
    geometry.computeMorphNormals();
    matt = new THREE.MeshLambertMaterial({
      emissive: 0x74DDFB,
      color: 0x74DDFB,
      morphTargets: true,
      morphNormals: true,
      specular: 0xffffff,
      shininess: 10,
      transparent: true,
      shading: THREE.SmoothShading,
      vertexColors: THREE.FaceColors
    });
    piDiAnimation1 = new THREE.MorphAnimMesh(geometry, matt);
    piDiAnimation1.rotateY(Math.PI)
    piDiAnimation1.parseAnimations();
    piDiAnimation1.scale.set(5, 5, 5);
    scene.add(piDiAnimation1);
  });
  loader.load('piDI/model/speak_final3.json', function (geometry, mat) {
    geometry.computeMorphNormals();
    matt = new THREE.MeshLambertMaterial({
      emissive: 0x74DDFB,
      color: 0x74DDFB,
      morphTargets: true,
      morphNormals: true,
      specular: 0xffffff,
      shininess: 10,
      transparent: true,
      shading: THREE.SmoothShading,
      vertexColors: THREE.FaceColors
    });
    piDiAnimation2 = new THREE.MorphAnimMesh(geometry, matt);
    piDiAnimation2.rotateY(Math.PI)
    piDiAnimation2.parseAnimations();
    piDiAnimation2.scale.set(5, 5, 5);
    scene.add(piDiAnimation2);
  });
}
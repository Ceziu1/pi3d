var socketio = io();
var loader = new THREE.JSONLoader();
var conf, modelDir, currentTexture;
var prevPosX = prevPosY = 0.5;
var file;

$(document).ready(function () {
	document.getElementById("video").ontimeupdate = function () {
		socketio.emit("currentTime", {
			time: parseInt(this.currentTime),
			duration: parseInt(this.duration)
		});
	}
})

socketio.on("ip", function (data) {
	initScene(data.ip)
});

socketio.on("showPiDI", function () {
	loadBasePiDI();
	PiDIsilenceAnimation();
})

socketio.on("hidePiDI", function () {
	closePiDI();
})

socketio.on("mediaPlayer", function (data) {
	switch (data.type) {
		case "pause":
			$("#video").get(0).pause()
			break;
		case "play":
			$("#video").get(0).play()
			break;
		// case "volume":
		// 	$("#video").get(0).muted = false
		// 	break;
		// case "mute":
		// 	$("#video").get(0).muted = true
		// 	break;
		// case "left":
		// 	$("#video").get(0).currentTime -= 10
		// 	break;
		// case "right":
		// 	$("#video").get(0).currentTime += 10
		// 	break;
		default:
			break;
	}
})

socketio.on("playPiDIdialogue", function (data) {
	console.log(data.file)
	$("#playerSource").attr("src", data.file);
	$("#player").trigger("load");
	$("#player").bind("loadeddata", function () {
		$("#player").unbind("loadeddata")
		$("#player").trigger("play");
		PiDIspeakAnimation();
	});
	$("#player").bind("ended", function () {
		$("#player").unbind("ended")
		if (data.file== "piDI/audio/Wyjdź.mp3" || data.file.indexOf("modelsAudio")>= 0) {
			closePiDI();
			return;
		}
		PiDIsilenceAnimation();
	})
})

socketio.on("updateInfo", function (data) {//AKTUALIZOWANIE OSWIETLENIA DLA MESHA
	// console.log(data.updateType + " " + data.info);
	switch (data.updateType) {
		case "zoom":
			// camera.rotation.z = 0;
			camera.rotation.z = -Math.PI/2
			camera1.rotation.z = 0;
			camera2.rotation.z = Math.PI/2
			camera.fov = -(conf.option.maxZoom - conf.option.minZoom) * data.info + conf.option.minZoom
			camera1.fov = -(conf.option.maxZoom - conf.option.minZoom) * data.info + conf.option.minZoom
			camera2.fov = -(conf.option.maxZoom - conf.option.minZoom) * data.info + conf.option.minZoom
			break;
		case "moveY":
			camera.position.y = conf.option.positionY * (data.info - 0.5) * 2
			camera1.position.y = conf.option.positionY * (data.info - 0.5) * 2
			camera2.position.y = conf.option.positionY * (data.info - 0.5) * 2
			break;
		case "moveX":
			camera.position.x = conf.option.positionZ * (data.info - 0.5) * 2
			camera1.position.x = conf.option.positionZ * (data.info - 0.5) * 2
			camera2.position.x = conf.option.positionZ * (data.info - 0.5) * 2
			break;
		case "rotate":
			for (var i = 0; i < conf.loader.length; i++) {
				scene.getObjectByName(conf.loader[i]).rotation.y = (360 * data.info) * (Math.PI / 180);
			}
			break;
		case "offset":
			if(conf.objects[currentTexture].offset)
				scene.getChildByName(conf.meshTexture).material.materials[0].map.offset.x = conf.objects[currentTexture].offsetX * data.info;
			break;
		default:
			break;
	}
})

socketio.on("updateTexture", function (data) {
	var textureToChange = scene.getChildByName(conf.meshTexture);
	for (var i = 0; i < conf.objects.length; i++) {
		if (conf.objects[i].name == data.texture.trim()) {
			textureToChange.material.materials[0].map = new THREE.ImageUtils.loadTexture(modelDir + "/" + conf.objects[i].src);
			textureToChange.geometry.faceVertexUvs = makeUvMap(conf.objects[i].uvs)
			textureToChange.geometry.uvsNeedUpdate = true;
			currentTexture= i;
			return;
		}
	}
})

socketio.on("renderConfig", function (data) {
	$("#video").trigger("pause");
	scene.children.length = 0;
	camera.position.z= camera2.position.z= -30;
	camera1.position.z = -15;
	conf = JSON.parse(data.config);
	var directionalLight = new THREE.PointLight(0xffffff, 2, 100);
	directionalLight.position.set(0, 20, -15);
	scene.add(directionalLight);
	modelDir = data.dirToMesh
	for (var i = 0; i < conf.loader.length; i++) {
		renderScene(modelDir + "/" + conf.loader[i], conf.loader[i]);
	}
})

socketio.on("renderConfigForUI", function (data) {
	$("#videoSource").attr("src", data.config);
	$("#video").trigger("load");
	$("#video").bind("loadeddata", function () {
		$("#video").unbind("loadeddata")
		$("#video").trigger("play");
		scene.children.length = 0;
		camera.fov= camera1.fov= camera2.fov = 65;
		camera.position.x= camera1.position.x= camera2.position.x= camera.position.y= camera1.position.y= camera2.position.y= 0;
		camera.rotation.z = Math.PI / 2;
    camera1.rotation.z = Math.PI;
    camera2.rotation.z = -Math.PI / 2;
		camera.position.z= camera2.position.z= -30;
		camera1.position.z = -15;
		var directionalLight = new THREE.PointLight(0xffffff, 2, 100);
		directionalLight.position.set(0, 20, -15);
		scene.add(directionalLight);

		videoImage = document.createElement( 'canvas' );
		videoImage.width = $("#video").get(0).videoWidth;
		videoImage.height = $("#video").get(0).videoHeight;
		videoImageContext = videoImage.getContext( '2d' );
		videoImageContext.fillStyle = '#000000';
		videoImageContext.fillRect( 0, 0, videoImage.width, videoImage.height );
		videoTexture = new THREE.Texture( videoImage );
		videoTexture.minFilter = THREE.LinearFilter;
		videoTexture.magFilter = THREE.LinearFilter;
		var movieMaterial = new THREE.MeshBasicMaterial( { map: videoTexture, overdraw: true, side:THREE.DoubleSide } );
		var movieGeometry = new THREE.PlaneGeometry( 32 , 18, 4, 4 );
		var movieScreen = new THREE.Mesh( movieGeometry, movieMaterial );
		movieScreen.position.set(0,0,0);
		scene.add(movieScreen);
		// camera.lookAt(movieScreen.position);
		// camera1.lookAt(movieScreen.position);
		// camera2.lookAt(movieScreen.position);
	});
})

function renderScene(path, id) {
	loader.load(path, function (geometry, material) {
		var materials = new THREE.MeshFaceMaterial(material);
		var object = new THREE.Mesh(geometry, materials);
		object.name = id;
		object.rotation.y = Math.PI;
		object.scale.set(10, 10, 10);
		scene.add(object);
	});
}

function makeUvMap(UV) {
	var arrayMap = [[[], []]];
	arrayMap[0][0].push(new THREE.Vector2(UV[0], UV[1]));
	arrayMap[0][0].push(new THREE.Vector2(UV[2], UV[3]));
	arrayMap[0][0].push(new THREE.Vector2(UV[6], UV[7]));
	arrayMap[0][1].push(new THREE.Vector2(UV[4], UV[3]));
	arrayMap[0][1].push(new THREE.Vector2(UV[2], UV[5]));
	arrayMap[0][1].push(new THREE.Vector2(UV[0], UV[7]));
	return arrayMap;
}
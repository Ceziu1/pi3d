var http = require('http');
var fs = require('fs');
var path = require('path');
var socketio = require("socket.io");
var ip = require("ip");
var rootDir = "";
/*-----------------------HTTP--------------------------*/
var httpServer = http.createServer(function (req, res) {
  switch (req.method) {
    case "GET":
      if (req.url == "/") req.url = "/index.html";
      else if (req.url == "/ui") req.url = "/dashboard.html";
      if (req.url != "/favicon.ico") {
        req.url = req.url.replace(/%20/g, " ");
        req.url = req.url.replace(/%C3%B3/g, "ó");
        req.url = req.url.replace(/%C5%BA/g, "ź");
        req.url = req.url.replace(/%C4%87/g, "ć");
        var filename = path.join(__dirname, req.url);
        console.log(req.url);
        var fileStream = fs.createReadStream(filename);
        fileStream.on('data', function (data) {
          res.write(data);
        });
        fileStream.on('end', function () {
          res.end();
        });
      }
      break;
  }
}).listen(3000);

/*-----------------------SOCKET.IO--------------------------*/
var io = socketio.listen(httpServer);
io.sockets.on("connection", function (client) { //Przy połączeniu do serwera wykonywany jest ten event
  client.on("ready", function (data) { //serwer dostaje wiadomość gracz zakonczył ture
    client.emit("ip", { //odesłanie ip karty sieciowej
      ip: ip.address()
    });
  });

  client.on("showPiDI", function () {
    io.emit("showPiDI", {})
  })

  client.on("hidePiDI", function () {
    io.emit("hidePiDI", {})
  })

  client.on("playPiDIdialogue", function (data) {
    io.emit("playPiDIdialogue", {
      file: data.file
    })
  })

  client.on("updateInfo", function (data) {
    io.emit("updateInfo", {
      updateType: data.updateType,
      info: data.data
    })
  })

  client.on("path", function (data) { //serwer dostaje wiadomość gracz zakonczył ture
    var tab = [];
    var i = 0;
    if (data.defaultDir == true) {
      rootDir = data.path
      fs.readdirSync(rootDir).forEach(function (item) {
        tab.push({
          file: fs.readdirSync(rootDir)[i],
          attr: fs.statSync(rootDir).isDirectory()
        })
        i++;
      })
    } else {
      rootDir += "/" + data.path
      fs.readdirSync(rootDir).forEach(function (item) {
        if (item.indexOf(".conf") >= 0 || item.indexOf(".mp4") >= 0) {
          tab.push({
            file: fs.readdirSync(rootDir)[i],
            attr: fs.statSync(rootDir + "/" + item).isDirectory()
          })
        }
        i++;
      })
    }
    client.emit("dirList", {
      list: tab
    });
  })

  client.on("updateTexture", function (data) {
    io.emit("updateTexture", {
      texture: data.texture
    })
  })

  client.on("upload", function (data) {
    if (data.config.indexOf(".mp4") >= 0) {
      io.emit("renderConfigForUI", {
        config: rootDir + "/" + data.config
      });
    } else {
      fs.readFile(rootDir + "/" + data.config, "utf8", function (err, data){
        if (err) throw err;
        client.emit("renderConfigForUI", {
          config: data
        });
        io.emit("renderConfig", {
          config: data,
          dirToMesh: rootDir
        });
      });
    }
  })

  client.on("currentTime", function(data){
    io.emit("currentTime", {
      time: data.time,
      duration: data.duration
    })
  })

  client.on("mediaPlayer", function(data){
    io.emit("mediaPlayer", {
      type: data.type,
    })
  })
})
